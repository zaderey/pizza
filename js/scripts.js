$.parallaxify();

$('.products__position-item').on('click', function() {
	var name = $(this).data('name');
	console.log(name);

	$('.products__position-item a').removeClass('active');
	$(this).find('a').addClass('active');
	$('.products__catalog').hide();
	$('#'+name).show();
});